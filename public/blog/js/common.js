$(function () {
    $(document).on('click','a[target!="_blank"]',function (ev) {
        if(history.pushState){
            ev.preventDefault();
            var url = $(ev.target).attr('href');
            if (url === location.pathname){
                return;
            }
            $.get(url).then(function (data) {
                if (data.code === 0){
                    alert(data.msg);
                    return;
                }
                var html = $(data).find('#pjax-container').html();
                var title = $(html).find('title').text();
                $('#pjax-container').html(html);
                document.title = title;
                window.history.pushState(200, title, url);
            })
        }
    })
})
<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 20:39
 */

namespace app\blog\controller;

use think\Controller;
use think\Request;
use app\common\model\Category as CategoryModel;
use \app\common\model\Article as ArticleModel;

/**
 * 分类控制器
 * Class Category
 * @package app\blog\controller
 */
class Category extends Controller
{
    public function index(Request $request)
    {
        $id = $request->route('id',false);
        !$id && $this->error("参数错误！");
        $category = CategoryModel::get($id);
        if (empty($category)){
            if ($request->isAjax()){
                $this->error('不存在的文章分类！');
            }else{
                abort(404);
            }
        }
        $this->assign('category',$category);
        $page = ArticleModel::where('state',1)
            ->where('cid',$id)
            ->where('is_deleted',0)
            ->order('id desc')
            ->paginate();

        $this->assign([
            'page'=>$page->render(),
            'list'=>$page->all()
        ]);
        return $this->fetch();
    }
}
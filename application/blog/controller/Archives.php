<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 20:37
 */

namespace app\blog\controller;

use think\Controller;
use think\Request;
use app\common\model\Article as ArticleModel;

class Archives extends Controller
{
    public function index(Request $request)
    {
        $page = ArticleModel::where('state',1)
            ->where('is_deleted',0)
            ->order('id desc')
            ->paginate();
        $this->assign([
            'page'=>$page->render(),
            'list'=>$page->all()
        ]);
        return $this->fetch();
    }
}
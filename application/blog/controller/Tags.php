<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 20:38
 */

namespace app\blog\controller;

use app\common\model\TagsMap;
use think\Controller;
use think\Request;
use \app\common\model\Article as ArticleModel;

/**
 * 标签控制器
 * Class Tags
 * @package app\blog\controller
 */
class Tags extends Controller
{
    public function index(Request $request)
    {
        $id = $request->route('id',false);
        !$id && $this->error("参数错误！");
        $tag = \app\common\model\Tags::get($id);
        if (empty($tag)){
            if ($request->isAjax()){
                $this->error('不存在的文章分类！');
            }else{
                abort(404);
            }
        }
        $this->assign('tag',$tag);
        $ids = TagsMap::where('tag_id',$id)->column('article_id');
        $page = ArticleModel::whereIn('id',$ids)
            ->where('state',1)
            ->where('is_deleted',0)
            ->order('id desc')
            ->paginate();

        $this->assign([
            'page'=>$page->render(),
            'list'=>$page->all()
        ]);
        return $this->fetch();
    }
}
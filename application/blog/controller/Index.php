<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/28
 * Time: 22:13
 */

namespace app\blog\controller;

use think\Controller;
use app\common\model\Article as ArticleModel;

/**
 * 前段首页控制器
 * Class Index
 * @package app\blog\controller
 */
class Index extends Controller
{
    public function index()
    {
        $page = ArticleModel::where('state',1)
            ->where('is_deleted',0)
            ->order('id desc')
            ->paginate();
        $this->assign([
            'page'=>$page->render(),
            'list'=>$page->all()
        ]);
        return $this->fetch();
    }
}
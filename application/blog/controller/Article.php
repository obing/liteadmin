<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 20:38
 */

namespace app\blog\controller;

use think\Controller;
use think\Request;
use \app\common\model\Article as ArticleModel;

/**
 * 文章控制器
 * Class Article
 * @package app\blog\controller
 */
class Article extends Controller
{
    public function index(Request $request)
    {
        $id = $request->route('id',false);
        !$id && $this->error('参数错误！');

        $article = ArticleModel::where('id',$id)
            ->where('state',1)
            ->where('is_deleted',0)
            ->find();
        if (empty($article)){
            if ($request->isAjax()){
                $this->error('不存在的文章！');
            }else{
                abort(404);
            }
        }
        $this->assign('article',$article);
        $article->click = $article->click+1;
        $article->save();
        return $this->fetch();
    }
}
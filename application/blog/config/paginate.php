<?php
/**
 * https://gitee.com/Mao02
 * http://www.mao02.win/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/11 0011
 * Time: 下午 21:43
 */
return [
	'type'      => 'app\common\lib\Bootstrap4',
	'var_page'  => 'page',
	'list_rows' => 10,
];
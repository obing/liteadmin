<?php
/**
 * https://gitee.com/Mao02
 * http://www.mao02.win/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/6 0006
 * Time: 上午 0:07
 */

namespace app\common\facade;

use think\Facade;

/**
 * @see \app\common\service\Tree
 * @mixin \app\common\service\Tree
 * @method mixed array2tree($array,$pk='id',$pkey='pid',$skey='child') static 数组转树型
 * @method mixed tree2list($tree,$skey='child',$prefix='_pre',$level=0) static 树型转数组
 * @method mixed array2list($array,$pk='id',$pkey='pid',$skey='child') static 数组转树形数组
 */
class Tree extends Facade
{
    protected static function getFacadeClass()
    {
        return \app\common\service\Tree::class;
    }
}
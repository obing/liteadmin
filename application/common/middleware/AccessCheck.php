<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 */
namespace app\common\middleware;

use app\common\facade\Auth;
use app\common\model\SystemAdmin;
use think\exception\HttpResponseException;
use think\Response;
use traits\controller\Jump;

/**
 * 权限校验中间件
 * Class AccessCheck
 * @package app\http\middleware
 */
class AccessCheck
{
    use Jump;
    /**
     * 执行入口
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $admin_id = session('admin.id');
        if (!!$admin_id){
            $admin = SystemAdmin::get($admin_id);
            if ($admin->getData('state') !== 1){
                abort(403, '当前账户已被禁用');
            }
        }

        $path = "{$request->module()}/{$request->controller()}/{$request->action()}";

        if (!Auth::auth($path))
            abort(403, '没有授权');

        return $next($request);
    }
}

<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/5/29
 * Time: 13:42
 */

namespace app\admin\controller;

use app\common\controller\BasicAdmin;
use think\facade\Request;

/**
 * @title 友情链接
 * Class Link
 * @package app\admin\controller
 */
class Link extends BasicAdmin
{
    protected $table = "ContentLink";

    /**
     * @title 列表页
     * @return array|mixed|\PDOStatement|string|\think\Collection
     */
    public function index(){
        return $this->_list();
    }

    /**
     * @title 添加
     * @return mixed
     */
    public function add()
    {
        return $this->_form('', 'form');
    }

    /**
     * @title 编辑
     * @return mixed
     */
    public function edit()
    {
        return $this->_form('', 'form');
    }

    protected function _form_before($data)
    {
        if (Request::isPost()){
            if (strpos($data['href'],'http') !== 0){
                $this->error("请以http://或https://开头");
            }
        }
    }

    /**
     * @title 删除
     * @throws \think\Exception
     */
    public function del()
    {
        $ids = Request::get('ids');
        $this->_del($ids);
    }

    /**
     * @title 禁用/启用操作
     * @throws \think\Exception
     */
    public function change()
    {
        $id = Request::get('id');
        $state = Request::get('state');
        $this->_change($id, ['state' => $state]);
    }
}
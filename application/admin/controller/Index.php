<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/1 0001
 * Time: 下午 22:33
 */

namespace app\admin\controller;

use app\common\controller\BasicAdmin;
use app\common\facade\Tree;
use think\App;
use think\Db;
use think\facade\Response;
use think\facade\Session;
use think\Request;

/**
 * @title 首页
 * Class Index
 * @package app\admin\controller
 */
class Index extends BasicAdmin
{
    /**
     * @title 首页
     * @auth 0
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $menus = Db::name('SystemMenu')
            ->order('sort', 'asc')
            ->select();
        $menus = Tree::array2tree($menus);
        // 过滤菜单匿名函数
        $func = function ($nodes) use (&$func){
            foreach ($nodes as $key => &$node){
                if (!empty($node['child'])){
                    $node['child'] = $func($node['child']);
                }
                if (strpos($node['url'],'#') === 0 ){
                    if (empty($node['child'])){
                        unset($nodes[$key]);
                    }
                }elseif (strpos($node['url'],'http://') === 0 || strpos($node['url'],'https://') === 0 ){
                    continue;
                }else{
                    if (!auth($node['url'])){
                        unset($nodes[$key]);
                    }else{
                        $node['url'] = url($node['url']);
                    }
                }
            }
            return $nodes;
        };
        $menus = $func($menus);
        return $this->fetch('', ['menus' => $menus]);
    }

    /**
     * @title 编辑
     * @auth 1
     * @return array|mixed|null|\PDOStatement|string|\think\Model
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit()
    {
        $this->table = 'SystemAdmin';
        return $this->_form('', 'form');
    }

    /**
     * 编辑前置
     * @param $data
     */
    public function _edit_form_before(&$data)
    {
        if (intval($data['id']) !== session('admin.id')){
            abort(403,'非法操作！');
        }
        (intval($data['id']) === 1) && abort(403,'超级用户禁止修改！');
    }

    /**
     *
     * @title 修改我的密码
     * @auth 1
     * @param Request $request
     * @return array|mixed|null|\PDOStatement|string|\think\Model
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function password(Request $request)
    {
        $this->table = 'SystemAdmin';
        return $this->_form('', 'password');
    }

    /**
     * 设置密码前置
     * @param $data
     */
    protected function _password_form_before(&$data)
    {
        if (\think\facade\Request::isPost()) {

            if (intval($data['id']) !== session('admin.id')){
                abort(403,'非法操作！');
            }

            $password = $data['password'];
            $repassword = $data['repassword'];

            (strlen($password) < 5 || strlen($password) > 25) && $this->error('密码长度必须5-25位之间');
            !preg_match('/^[a-zA-Z0-9]+$/', $password) && $this->error('只能使用字母数字');
            ($password !== $repassword) && $this->error('两次密码输入不一致');
            unset($data['repassword']);
            $option = [
                'cost'=>config('password.cost')
            ];
            $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT, $option);
        }
    }
    
    /**
     * @title 欢迎页
     * @auth 0
     */
    public function welcome()
    {
        App::VERSION;
        return $this->fetch();
    }
}

<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/1 0001
 * Time: 下午 23:13
 */

namespace app\admin\controller;

use app\common\controller\BasicAdmin;
use app\common\facade\Tree;
use think\Db;
use think\facade\Request;

/**
 * @title 菜单管理
 * Class Menu
 * @package app\admin\controller
 */
class Menu extends BasicAdmin
{
    
    protected $table = 'SystemMenu';
    
    /**
     * @title 列表页
     * @return mixed
     */
    public function index()
    {
        $db = Db::name($this->table)
            ->order('sort', 'asc')
            ->order('id', 'asc');
        return $this->_list($db, false);
    }
    
    /**
     * 列表前置
     * @param $list
     */
    public function _index_list_before(&$list)
    {
        // 匿名函数 整理子菜单IDs 用于删除时全部子菜单同时删除
        $func = function (&$tree) use (&$func) {
            $aids = [];
            foreach ($tree as &$item) {
                $ids = [];
                $ids[] = $item['id'];
                if (isset($item['child'])) {
                    $ids = array_merge($ids, $func($item['child']));
                }
                $aids = array_merge($aids, $ids);
                $item['son_ids'] = implode(',', $ids);
            }
            return $aids;
        };
        $tree = Tree::array2tree($list);
        $func($tree);
        $list =  Tree::array2list($tree);
    }

    /**
     * @title 添加
     * @return mixed
     * @return array|mixed|null|\PDOStatement|string|\think\Model
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function add()
    {
        return $this->_form('', 'form');
    }

    /**
     * @title 编辑
     * @return mixed
     * @return array|mixed|null|\PDOStatement|string|\think\Model
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit()
    {
        return $this->_form('', 'form');
    }

    /**
     * 表单前置
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function _form_before($data)
    {
        if (\think\facade\Request::isGet()) {
            $db = Db::name($this->table);
            $id = \think\facade\Request::get('id', false);
            $id && $db->whereNotIn('id', $id);
            $parents = $db
                ->order('sort asc,id asc')
                ->select();
            $parents = Tree::array2list($parents, 'id', 'pid', 'child');
            $pid = \think\facade\Request::get('pid', false);
            if ($pid){
                $this->assign('pid', $pid);
            }
            $this->assign('parents', $parents);
        }
    }

    /**
     * @title 删除菜单
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function del()
    {
        $ids = Request::get('ids');
        $res = Db::name($this->table)->whereIn('id', $ids)->delete();
        if ($res) {
            $this->success('删除成功！', '');
        } else {
            $this->error("删除失败");
        }
    }
}
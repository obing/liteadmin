<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/5/30
 * Time: 16:31
 */

namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\exception\PDOException;
use think\Request;
use think\Response;

/**
 * @title 上传
 * Class Upload
 * @package app\admin\controller
 */
class Upload extends Controller
{
    /**
     * @title 文件上传
     * @auth 1
     *          通用文件上传，校验MD5快速重传
     * @param Request $request
     * @param Response $response
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function file(Request $request, Response $response)
    {
        $file = current($request->file());
        $res = Db::name('ContentAttachment')->where('hash','=',$file->hash('md5'))->find();
        if (!empty($res)){
            return json(['code'=>0,'msg'=>'上传成功','data'=>['src'=>$res['path']]]);
        }
        // 移动到框架应用根目录/uploads/ 目录下
        $dir = env('root_path').DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'uploads';
        $info = $file->move( $dir);
        if($info){
            $hash = $info->hash('md5');
            $savename = '/uploads/'.str_replace('\\','/',$info->getSaveName());

            $data = [
                'hash'=>$hash,
                'path'=>$savename,
                'create_time'=>$request->time(),
                'size'=>$info->getInfo('size')
            ];

            try{
                Db::name('ContentAttachment')->insert($data);
            }catch (PDOException $e){
                return json(['code'=>1,'msg'=>$e->getMessage()]);
            }
            return json(['code'=>0,'msg'=>'上传成功','data'=>['src'=>$savename]]);

        }else{
            return json(['code'=>1,'msg'=>$info->getError()]);

        }
    }

    /**
     * @title 检查图片是否存在
     * @auth 1
     * @param Request $request
     * @return Response|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function checkFile(Request $request)
    {
        $hash = $request->get('hash');
        $file = Db::name('ContentAttachment')->where('hash','=',$hash)->find();

        if (!empty($file)){
            return json(['code'=>0,'msg'=>'上传成功','data'=>['src'=>$file['path']]]);
        }else{
            return json(['code'=>1,'msg'=>'文件不存在']);
        }
    }

    /**
     * @title 百度umeditor富文本编辑器图片插入
     * @auth 1
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function ueditor(Request $request)
    {
        $file = current($request->file());
        $res = Db::name('ContentAttachment')->where('hash','=',$file->hash('md5'))->find();
        if (!empty($res)){
            $json = [
                "originalName" => pathinfo($res['path'],PATHINFO_BASENAME) ,
                "name" => pathinfo($res['path'],PATHINFO_BASENAME) ,
                "url" => $res['path'],
                "size" => $res['size'] ,
                "type" => pathinfo($res['path'],PATHINFO_EXTENSION) ,
                "state" => "SUCCESS"
            ];
            return json($json)->header('Content-Type','text/html');
        }
        // 移动到框架应用根目录/uploads/ 目录下
        $dir = env('root_path').DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'uploads';
        $info = $file->move( $dir);
        if($info){
            $hash = $info->hash('md5');
            $savename = '/uploads/'.str_replace('\\','/',$info->getSaveName());

            $data = [
                'hash'=>$hash,
                'path'=>$savename,
                'create_time'=>$request->time(),
                'size'=>$info->getInfo('size')
            ];

            try{
                Db::name('ContentAttachment')->insert($data);
            }catch (PDOException $e){
                $json = [
                    "state" => $e->getMessage()
                ];
                return json($json);
            }
            $json = [
                "originalName" => $info->getFilename(),
                "name" => $info->getFilename(),
                "url" => $savename,
                "size" => $info->getInfo('size') ,
                "type" => '.'.$info->getExtension() ,
                "state" => "SUCCESS"
            ];
            return json($json)->header('Content-Type','text/html');
        }else{
            $json = [
                "state" => $info->getError()
            ];
            return json($json)->header('Content-Type','text/html');
        }
    }

    /**
     * @title wangEditor富文本编辑器图片插入
     * @auth 1
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function wangeditor(Request $request)
    {
        $file = current($request->file());
        $res = Db::name('ContentAttachment')->where('hash','=',$file->hash('md5'))->find();
        if (!empty($res)){
            $json = [
                "errno" => 0,
                "data" => [
                    $res['path']
                ]
            ];
            return json($json);
        }
        // 移动到框架应用根目录/uploads/ 目录下
        $dir = env('root_path').DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'uploads';
        $info = $file->move( $dir);
        if($info){
            $hash = $info->hash('md5');
            $savename = '/uploads/'.str_replace('\\','/',$info->getSaveName());

            $data = [
                'hash'=>$hash,
                'path'=>$savename,
                'create_time'=>$request->time(),
                'size'=>$info->getInfo('size')
            ];

            try{
                Db::name('ContentAttachment')->insert($data);
            }catch (PDOException $e){
                $json = [
                    "errno" => 1,
                    "meg" => $e->getMessage()
                ];
                return json($json);
            }
            $json = [
                "errno" => 0,
                "data" => [
                    $savename
                ]
            ];
            return json($json);
        }else{
            $json = [
                "errno" => 1,
                "msg" => $info->getError()
            ];
            return json($json);
        }
    }

    /**
     * @title markdown编辑器图片插入
     * @auth 1
     */
    public function markdown(Request $request)
    {
        $file = $request->file('editormd-image-file');
        $res = Db::name('ContentAttachment')->where('hash','=',$file->hash('md5'))->find();
        if (!empty($res)){
            $json = [
                "success" => 1,
                "url" => $res['path']
            ];
            return json($json);
        }
        // 移动到框架应用根目录/uploads/ 目录下
        $dir = env('root_path').DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'uploads';
        $info = $file->move( $dir);
        if($info){
            $hash = $info->hash('md5');
            $savename = '/uploads/'.str_replace('\\','/',$info->getSaveName());

            $data = [
                'hash'=>$hash,
                'path'=>$savename,
                'create_time'=>$request->time(),
                'size'=>$info->getInfo('size')
            ];

            try{
                Db::name('ContentAttachment')->insert($data);
            }catch (PDOException $e){
                $json = [
                    "success" => 0,
                    "message" => $e->getMessage()
                ];
                return json($json);
            }
            $json = [
                "success" => 1,
                "url" => $savename
            ];
            return json($json);
        }else{
            $json = [
                "success" => 0,
                "message" => $info->getError()
            ];
            return json($json);
        }

    }
}